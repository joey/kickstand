const SiteGenerator = require("./lib/SiteGenerator")
const {File, Folder} = require("./lib/Dir")
module.exports = {SiteGenerator, File, Folder}